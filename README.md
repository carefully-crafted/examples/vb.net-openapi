# Motivation

Diese Beispiel zeigt, wie die API der Fahrer-App, das sog. _erp-gateway_ aus VisualBasic aufgerufen werden kann. 

Die API ist mittels OpenAPI dokumentiert. Deshalb können Clients für zahlreiche Programmiersprachen automatisch generiert werden. 

## Konfiguration

Wir betreiben das _erp-gateway_  in zwei Umgebungen. _Development_  und _Production_. Beide Umgebungen sind komplett unabhängig und haben 
demzufolge auch unterschiedliche Konfigurationswerte und Secrets.

Benötigt wird folgende Konfiguration: 

| Parameter     | Development | Production |
| ------------- | ------------- | ----- |
| Base-URL      | https://hkh-dev.ew.r.appspot.com/erp-gateway | https://hkh-prod.ew.r.appspot.com/erp-gateway |
| API-Key       | `AIzaSyCwsTb4eOXxCrbLWvdi62QzBiJVDBzzYZo`    | `AIzaSyDXQNM7rsfkxqUiH7o5JBKut0jDGJ4czJ0`     |
| Audience      | `102232223605-949sk908vpg3lknhh6kje0g0lukaa3tt.apps.googleusercontent.com` | `981581041388-bpn12vphio5g9ui4q4l4hl6fl6egcgbi.apps.googleusercontent.com` |

Die API-Dokumentation ist bei Bedarf unter https://hkh-dev.ew.r.appspot.com/erp-gateway/api-docs verfügbar. 

### Secrets

- Pro Organisation (d.h. pro Firma / Zweigstelle, etc.) wird ein individueller Service Account benötigt. Die jeweilige JSON-Datei mit dem Private-Key
des Accounts stellen wir zur Verfügung. 
- Für den Zugriff auf die API-Dokumentation wird ein Nutzeraccount benötigt, der ebenfalls zur Verfügung gestellt wird. 

## Beispiel-Code

Diese Solution besteht aus zwei Projekten: 

1. `ErpGateway`: In diesem Projekt befindet sich ausschließlich der autogenerierte OpenAPI-Client
2. `WebClientTest`: In diesem Projekt befindet sich Beispielcode für: 
    - Authentifizierung mittels einer Service-Account Datei
    - Instanziierung des API-Clients
    - Aufruf eines Endpunktes (in diesem Fall das Anlegen von Touren.)

### Unchase OpenAPI Connected Service

Zur Generierung des C# Clients wurde [Unchase OpenAPI Connected Service](https://marketplace.visualstudio.com/items?itemName=Unchase.unchaseopenapiconnectedservice) benutzt.
Ein Tutorial, wie man mit Unchase einen Client generiert findet man im vorstehenden Link. 

Der Client muss in eine C# Klassenbibliothek (mit gleicher .NET-Version wie das VB.NET Projekt) generiert werden. Diese wird als Referenz zum VB.NET Projekt hinzugefügt und 
kann dann reibungslos verwendet werden (Siehe `WebClientTest/Module1.vb`). 

Der Client wurde aus der dem Projekt hinzugefügten `swagger.json`-OpenAPI Spezifikation generiert. Eine stets aktuelle `swagger.json` kann unter 
https://hkh-dev.ew.r.appspot.com/erp-gateway/api-docs/swagger.json heruntergeladen werden. 

## Nutzung der API 

Die API hat folgende Designziele: 

- alle `PUT`-Endpunkte sind idempotent. Das heißt, dass identische Requests beliebig oft wiederholt werden können, ohne dass Daten mehrfach angelegt werden. 
- alle `PUT`-Endpunkte geben bei Erfolg den HTTP-Status `201 Created` zurück. Im Fehlerfall ist wie folgt zu verfahren: 
    - HTTP-Status 5xx: Temporärer Serverfehler. Request sollte wiederholt werden. 
    - HTTP-Status 4xx: Request ist fehlerhaft und auch ein wiederholter Request wird den gleichen Fehlercode hervorrufen.

Die Endpunkte nehmen in der Regel eine Liste von Daten entgegen. Es macht Sinn, bei großen Datenmengen die Requests zu batchen. Wir haben mit
einer Batch-Größe von 100 Einträgen pro Request gute Erfahrung gemacht. 

## Verwendung der Environments

- Zunächst ist das Development-Environment zu verwenden. 
- Die Anbindung sollte idealerweise so gebaut werden, dass nach Möglichkeit mehrere Exporte zu unterschiedlichen Systemen mit 
  unterschiedlichen Accounts laufen können (z.B. aus einem System ein Export sowohl nach DEV, als auch nach PROD mit je unterschiedlicher 
  Konfiguration).