Imports IdentityModel.Client
Imports System.Net.Http
Imports System.IO
Imports ErpGateway.ErpGateway

Module Module1
    Sub Main()
        ' Der Cursor zeigt an, welche OrderChanges Winmeals bereits gelesen bzw. verarbeitet hat. 
        ' Dieser sollte abgespeichert werden (z.B. in der Datenbank, oder als Datei) und beim n�chsten
        ' Aufruf wieder gelesen werden. 
        Dim cursor As Double = 0

        ' Der Aufruf erfolgt Async...
        Dim task = RequestOrderChanges(cursor)
        task.Wait()

        ' R�ckgabewert ist der Cursor f�r die n�chste Abfrage...
        Dim nextCursor = task.Result

        Console.WriteLine("Next Cursor: " + nextCursor.ToString())

        Console.ReadLine()
    End Sub

    Private Async Function RequestOrderChanges(startFromCursor As Double) As Task(Of Double)
        ' Parameter k�nnen aus einer Datei geladen werden. 
        Dim localConfig = Config.ReadFromFile("dev.config.json")
        Dim client As Client = Await NewClient(localConfig)

        Dim cursor As Double = startFromCursor

        Try
            Dim isMoreDataAvailable = True
            Do While isMoreDataAvailable
                Dim response = Await client.GetOrdersAsync(cursor)
                cursor = response.NextCursor
                isMoreDataAvailable = response.IsMoreDataAvailable

                For Each change In response.Changes
                    HandleNewOrderChange(change)
                Next
                Console.WriteLine("> Cursor: " + cursor.ToString())
            Loop
        Catch e As ApiException
            Console.WriteLine("> Error: " + e.Message)
            Throw e
        End Try

        Return cursor
    End Function

    ' TODO: Hier m�ssen die OrderChanges irgendwie verarbeitet werden, z.B. in die Datenbank geschrieben
    ' werden.
    Private Sub HandleNewOrderChange(change As OutgoingOrderChange)
        Console.WriteLine(change.CustomerId + ": " + change.DeliveryDate.ToString())
    End Sub

    ' Erstellt einen neuen und frisch authentifizierten API-Client
    Private Async Function NewClient(config As Config) As Task(Of Client)
        Dim serviceAccountFile = New FileStream(config.ServiceAccountFilePath, FileMode.Open)

        ' Authenticate with Google Service Account
        Dim idToken = Await ServiceAccountAuth.GetToken(serviceAccountFile, config.TargetAudience, config.ApiKey)

        ' Initialize a HttpClient with the tok2en
        Dim httpClient = New HttpClient()
        httpClient.SetBearerToken(idToken)

        ' Create an instance of the auto-generated client
        Dim client = New Client(httpClient) With {
            .BaseUrl = config.BaseUrl
        }
        Return client
    End Function
End Module
