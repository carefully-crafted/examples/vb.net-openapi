﻿Imports System.Text.Json
Imports System.Text.Json.Serialization

Public Class Config
    <JsonPropertyName("targetAudience")>
    Public Property TargetAudience As String

    <JsonPropertyName("apiKey")>
    Public Property ApiKey As String

    <JsonPropertyName("serviceAccountFilePath")>
    Public Property ServiceAccountFilePath As String

    <JsonPropertyName("baseUrl")>
    Public Property BaseUrl As String

    Public Shared Function ReadFromFile(path As String) As Config
        Dim json As String = My.Computer.FileSystem.ReadAllText(path)
        Return JsonSerializer.Deserialize(Of Config)(json)
    End Function
End Class
