﻿Imports System.IO
Imports System.Net.Http
Imports System.Text.Json
Imports System.Text.Json.Serialization
Imports Google.Apis.Auth.OAuth2

Public Class ServiceAccountAuth
    Public Shared Async Function GetToken(serviceAccountFile As FileStream, targetAudience As String, apiKey As String) As Task(Of String)
        Dim credential = ServiceAccountCredential.FromServiceAccountData(serviceAccountFile)


        Dim token = Await credential.GetOidcTokenAsync(OidcTokenOptions.FromTargetAudience(targetAudience))
        Dim accessToken = Await token.GetAccessTokenAsync()

        Dim url = $"https://identitytoolkit.googleapis.com/v1/accounts:signInWithIdp?key={apiKey}"
        Dim request = New HttpRequestMessage(HttpMethod.Post, url) With {
            .Content = New StringContent($"
            {{
                ""postBody"": ""id_token={accessToken}&providerId=google.com"",
                ""requestUri"": ""http://localhost"",
                ""returnIdpCredential"": true,
                ""returnSecureToken"": true
            }}")
        }
        Dim httpClient = New HttpClient()
        Dim response = Await httpClient.SendAsync(request)
        If Not response.IsSuccessStatusCode Then
            Throw New Exception("Request Failed: " + Await response.Content.ReadAsStringAsync())
        End If
        Dim body = Await response.Content.ReadAsStringAsync()
        Return JsonSerializer.Deserialize(Of IdpResponse)(body).IdToken
    End Function
End Class

Public Class IdpResponse
    Private _idToken As String

    <JsonPropertyName("idToken")>
    Public Property IdToken As String
        Get
            Return _idToken
        End Get
        Set(value As String)
            _idToken = value
        End Set
    End Property
End Class
